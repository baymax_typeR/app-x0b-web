<?php
	session_start();
	include 'koneksi.php';
?>
<!DOCTYPE html>
<html>
<head>
 	<title>Akademi Konoha</title>
 	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.css">
 	<link rel="stylesheet" type="text/css" href="admin/assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body bgcolor="red">

<?php include 'menu.php' ?>
 		<!--konten -->
 		<section class="konten">
 			<div class="container">
			 	<br>
 				<h4 align='center'>Data Mahasiswa</h4>
				<br>

 				<div class="row">

 					<?php $ambil = $koneksi->query("SELECT a.nim, a.nama ,b.nama_prodi ,a.photos FROM mahasiswa a , prodi b  WHERE a.`id_prodi`= b.`id_prodi` ORDER BY b.`nama_prodi`");?>
 					<?php while($mhs = $ambil->fetch_assoc()){?>
 						<div class="col-md-3">
 							<div class="thumbnail">
 								<img src="../images/<?php echo $mhs['photos']; ?>" alt="">
 								<div class="caption">
 									<h4 align="left">Nama Mahasiswa: <?php echo $mhs['nama']; ?></h3>
 									<a href="detail.php?nim=<?php echo $mhs['nim'];?>" class="btn btn-info center-block">Detail</a>
 									<a class="btn btn-danger center-block" href="hapus.php?nim=<?php echo $mhs['nim']?>">Hapus</a>
 								</div>
 							</div>
 						</div>		
 					<?php } ?>
 				</div>
 			</div>
 		</section>

	  	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>